/* eslint-disable no-useless-return */
/* eslint-disable consistent-return */
/* eslint-disable max-len */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    tasks: [],
    modifiedTasks: [],
    currentTask: 0,
  },
  getters: {
    TASKS: (state) => state.tasks,
    MODIFIED_TASKS: (state) => state.modifiedTasks,
    TASK: (state, currentTask) => state.tasks[currentTask],
  },
  mutations: {
    SET_TASKS: (state, payload) => {
      state.tasks = [...payload];
      state.modifiedTasks = [...payload];
    },
    SET_MODIFIED_TASKS: (state, payload) => {
      state.modifiedTasks = [...payload];
    },
    SET_CURRENT_TASK: (state, payload) => {
      state.currentTask = payload;
    },
  },
  actions: {
    SET_TASKS: async (context) => {
      const response = await fetch('http://localhost:3001/tasks');
      const data = await response.json();
      const tasks = [...data.tasks];
      context.commit('SET_TASKS', tasks);
    },
    SET_MODIFIED_TASKS: (context, payload, state) => {
      const localTasks = [...state.tasks];
      let modedLocalTasks;
      switch (payload) {
        case 'importance':
          modedLocalTasks = { ...localTasks.sort((a, b) => a.importance - b.importance) };
          context.commit('SET_MODIFIED_TASKS', modedLocalTasks);
          break;
        case 'description':
          modedLocalTasks = { ...localTasks.sort((a, b) => a.description.toLowerCase() - b.description.toLowerCase()) };
          context.commit('SET_MODIFIED_TASKS', modedLocalTasks);
          break;
        case 'name':
          modedLocalTasks = { ...localTasks.sort((a, b) => a.name.toLowerCase() - b.name.toLowerCase()) };
          context.commit('SET_MODIFIED_TASKS', modedLocalTasks);
          break;
        default:
          context.commit('SET_MODIFIED_TASKS', state.tasks);
          break;
      }
    },
    SET_FIND: (context, payload, state) => {
      const localTasks = [...state.tasks];
      const elements = localTasks.reduce((tasks, task) => {
        if (task.name.includes(payload)) {
          return tasks.push(task);
        }
        if (task.description.includes(payload)) {
          return tasks.push(task);
        }
        if (task.worker.includes(payload)) {
          return tasks.push(task);
        }
        if (task.customer.includes(payload)) {
          return tasks.push(task);
        }

        return;
      }, []);
      context.commit('SET_MODIFIED_TASKS', elements);
    },
  },
  modules: {
  },
});
