const paths = [
  {
    text: 'Главная',
    path: '/',
  },
  {
    text: 'Задачи',
    path: '/tasks',
  },
];

export default { paths };
