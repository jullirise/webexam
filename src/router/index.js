import Vue from 'vue';
import VueRouter from 'vue-router';
import MainPage from '../views/pages/main-page.vue';
import TasksPage from '../views/pages/tasks-page.vue';
import TaskPage from '../views/pages/task-page.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: MainPage,
  },
  {
    path: '/tasks',
    name: 'tasks',
    component: TasksPage,
  },
  {
    path: '/signin',
    name: 'signin',
    component: TasksPage,
  },
  {
    path: '/signup',
    name: 'signup',
    component: TasksPage,
  },
  {
    path: '/tasks/:id',
    name: 'task',
    component: TaskPage,
    props: (route) => ({
      profileId: route.params.id,
    }),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
